// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_ENUM_ESITYPE
#define EVOL_MAP_ENUM_ESITYPE

#ifndef OLD_SI_MAX
#define OLD_SI_MAX 0
#error "vars.sh did not define OLD_SI_MAX"
#endif

// Nothing to do here! Deprecated

#endif  // EVOL_MAP_ENUM_ESITYPE
